package main

import (
	"log/slog"
	"net/http"

	"gitlab.com/trazolabs/harvest/v2"
	"gitlab.com/trazolabs/harvest/v2/harvestx"
)

type RootController struct {
}

func (c *RootController) Hello() *harvest.View {
	return harvest.Get("/hello", func(r *harvest.Request, _ any) (any, error) {
		return map[string]string{
			"Message": "Hello, world!",
		}, nil
	})
}

func main() {
	app := harvest.New(
		harvest.WithLogAttrs(slog.String("env", "dev")),
	)

	app.AddMiddleware(harvestx.HTTPLoggerMiddleware(false))

	app.AddMiddleware(func(r *harvest.Request) (any, error) {
		r.Logger().Info("this is a message from middleware 1")
		return r.Next()
	})

	app.AddMiddleware(func(r *harvest.Request) (any, error) {
		r.Logger().Info("this is a message from middleware 2")

		response, err := http.Get("https://api.sampleapis.com/beers/ale")
		return &harvest.ProxyResponse{
			Response: response,
		}, err
	})

	app.AddController("", &RootController{})
	app.Serve()
}
