package main

import (
	"bytes"
	_ "embed"
	"io"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/trazolabs/harvest/v2"
)

type Controller struct {
	app *harvest.App
}

type CreateInput struct {
	UUID   uuid.UUID
	ValueA string `desc:"this is value a"`
	ValueB int    `desc:"this is value b"`
}

type CreateOutput struct {
	ValueC string
	ValueD int

	Items []CreateOutput
}

func (c *Controller) Create() *harvest.View {
	return harvest.Post("/{UUID}", func(r *harvest.Request, input CreateInput) (*CreateOutput, error) {
		return nil, nil
	},
		harvest.WithSummary("Create a thing"),
		harvest.WithTags("Things"),
		harvest.WithPathParam("UUID", "The UUID"),
		harvest.WithQueryParam("Name", "A random name", false),
		harvest.WithDescription("TEST"),
	)
}

func (c *Controller) Detail() *harvest.View {
	return harvest.Get("", func(r *harvest.Request, _ any) ([]*CreateOutput, error) {
		return []*CreateOutput{}, nil
	},
		harvest.WithSummary("Get a thing"),
		harvest.WithTags("Things"),
	)
}

func (c *Controller) Inline() *harvest.View {
	return harvest.Get("/inline", func(r *harvest.Request, input struct {
		Value1 string
	}) ([]*CreateOutput, error) {
		return []*CreateOutput{}, nil
	},
		harvest.WithSummary("Get a thing"),
		harvest.WithTags("Things"),
	)
}

func (c *Controller) Spec() *harvest.View {
	return harvest.Get("/spec.json", func(r *harvest.Request, _ any) (*harvest.FileResponse, error) {
		spec, err := c.app.OpenAPISpec()
		if err != nil {
			return nil, err
		}

		return &harvest.FileResponse{
			ContentType: "application/json",
			Writer: func(writer io.Writer) error {
				_, err := bytes.NewBuffer(spec).WriteTo(writer)
				return err
			},
		}, nil
	})
}

//go:embed introduction.md
var introductionMarkdown string

func main() {
	app := harvest.New(harvest.WithOpenAPI(harvest.OpenAPIInfo{
		Title:   "Harvest Sample",
		Version: "0.0.1",
		Tags: []harvest.OpenAPIInfoTag{
			{
				Name:     "Introduction",
				Markdown: introductionMarkdown,
			},
		},
	}))

	app.AddController("/root", &Controller{app: app})

	app.Serve()
}
