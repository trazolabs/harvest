package main

import (
	"gitlab.com/trazolabs/harvest/v2"
	"gitlab.com/trazolabs/harvest/v2/harvestx"
)

type Parent struct {
	Name string

	harvestx.Include
}

type Child struct {
	Name string
}

type RootController struct {
}

func (c *RootController) List() *harvest.View {
	return harvest.Get("/list", func(r *harvest.Request, _ any) (any, error) {
		return &Parent{Name: "Parent"}, nil
	})
}

func (c *RootController) Page() *harvest.View {
	return harvest.Get("/page", func(r *harvest.Request, _ any) (any, error) {
		return &harvest.PageResponse[*Parent]{
			Count: 10,
			Items: []*Parent{{Name: "Parent"}},
		}, nil
	})
}

type ParentIncluder struct {
}

func (in *ParentIncluder) Children() harvestx.IncludeFunc {
	return func(r *harvest.Request, obj any) (any, error) {
		return []*Child{
			{Name: "Child 1"},
			{Name: "Child 2"},
		}, nil
	}
}

func main() {
	app := harvest.New()

	includeMiddleware := harvestx.NewIncludeMiddleware()
	includeMiddleware.AddIncluder(&Parent{}, &ParentIncluder{})
	app.AddMiddleware(includeMiddleware.Middleware())

	app.AddController("", &RootController{})

	app.Serve()
}
