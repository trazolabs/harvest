package main

import (
	"fmt"
	"net/http"
	"time"

	"gitlab.com/trazolabs/harvest/v2"
	"gitlab.com/trazolabs/harvest/v2/harvestx"
)

type EchoController struct {
}

type EchoOutput struct {
	AttrA string
	AttrB int
}

func (c *EchoController) Empty() *harvest.View {
	return harvest.Get("/empty", func(r *harvest.Request, _ any) (any, error) {
		return map[string]string{
			"Hey": "How are you",
		}, nil
	})
}

func (c *EchoController) World() *harvest.View {
	return harvest.Post("", func(r *harvest.Request, input struct {
		AttrA string
		AttrB int
	}) (*EchoOutput, error) {
		return &EchoOutput{
			AttrA: input.AttrA,
			AttrB: input.AttrB,
		}, nil
	})
}

func (c *EchoController) Error() *harvest.View {
	return harvest.Get("/error", func(r *harvest.Request, _ any) (any, error) {
		return nil, harvest.WithData(harvest.NewError(http.StatusNotFound, fmt.Errorf("my error")), map[string]any{
			"something": "here",
			"a":         1,
			"b":         true,
		})
	})
}

func (c *EchoController) Graceful() *harvest.View {
	return harvest.Get("/graceful", func(r *harvest.Request, _ any) (any, error) {
		fmt.Println("HERE!")
		time.Sleep(10 * time.Second)
		return nil, fmt.Errorf("works!")
	})
}

func main() {
	app := harvest.New()
	app.AddMiddleware(harvestx.HTTPLoggerMiddleware(false))
	app.AddController("/echo", &EchoController{})
	app.GracefulServe()
}
