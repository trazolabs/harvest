package main

import (
	"net/http"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/trazolabs/harvest/v2"
	"gitlab.com/trazolabs/harvest/v2/harvestdb"
	"gitlab.com/trazolabs/harvest/v2/harvestx"
)

type Thing struct {
	harvestdb.UUIDField
	Name string

	SubThings []*SubThing `gorm:"foreignKey:ThingUUID" json:"-"`

	harvestx.Include
}

type SubThing struct {
	harvestdb.UUIDField

	ThingUUID uuid.UUID `fk:"Things,UUID,CASCADE,CASCADE"`
	Name      string
}

type RootController struct {
}

type FilterResponse struct {
	MainThing *Thing
	SubThings []*SubThing
}

func (c *RootController) Filter() *harvest.View {
	return harvest.Get("", func(r *harvest.Request, _ any) ([]*FilterResponse, error) {
		response, err := harvestdb.Filter[Thing](r, r.Vars().F(), harvestx.MultiIncluderPreload(r, "SubThings", "SubThings"))
		if err != nil {
			return nil, err
		}

		responses := make([]*FilterResponse, 0)
		for _, thing := range response {
			subThigns := thing.SubThings
			thing.SubThings = nil

			responses = append(responses, &FilterResponse{
				MainThing: thing,
				SubThings: subThigns,
			})
		}

		return responses, nil
	})
}

func (c *RootController) Detail() *harvest.View {
	return harvest.Get("/{UUID}", func(r *harvest.Request, _ any) (*Thing, error) {
		return harvestdb.Detail[Thing](r, r.Vars().W())
	})
}

func (c *RootController) Redirect() *harvest.View {
	return harvest.Get("/redirect", func(r *harvest.Request, _ any) (any, error) {
		return &harvest.RedirectResponse{
			URL:        "https://example.org",
			StatusCode: http.StatusSeeOther,
		}, nil
	}, harvest.WithSummary("Redirect to another page"))
}

func (c *RootController) Plain() *harvest.View {
	return harvest.Get("/plain", func(r *harvest.Request, _ any) (any, error) {
		return &harvest.PlainResponse{
			Data: []byte("this is a plain response"),
		}, nil
	})
}

type ThingIncluder struct {
}

func (inc *ThingIncluder) All() harvestx.IncludeFunc {
	return func(r *harvest.Request, obj any) (any, error) {
		thing := obj.(*Thing)
		return thing.SubThings, nil
	}
}

func (inc *ThingIncluder) SubThings() harvestx.IncludeFunc {
	return func(r *harvest.Request, obj any) (any, error) {
		thing := obj.(*Thing)
		return thing.SubThings, nil
	}
}

func (inc *ThingIncluder) Calculate() harvestx.IncludeFunc {
	return func(r *harvest.Request, obj any) (any, error) {
		return "calculation!!!", nil
	}
}

func main() {
	db := harvestdb.Create("host=localhost port=5432 user=postgres password=postgres dbname=postgres sslmode=disable",
		harvestdb.WithMaxOpenConnections(100),
		harvestdb.WithLogMode(true),
		harvestdb.WithModels(&Thing{}, &SubThing{}),
	)
	defer db.Close()

	app := harvest.New(
		harvest.WithDatabase(db),
	)

	// thing := &Thing{Name: "Thing #1"}
	// db.Create(thing)
	// db.Create(&SubThing{ThingUUID: thing.UUID, Name: "Subthing #1.1"})
	// db.Create(&SubThing{ThingUUID: thing.UUID, Name: "Subthing #1.2"})

	gormIncluder := harvestx.NewGormIncluder()
	gormIncluder.AddIncluder(&Thing{}, &ThingIncluder{})
	gormIncluder.Register(db)

	app.AddController("/root", &RootController{})
	app.Serve()
}
