package main

import (
	"io"
	"net/http"

	"gitlab.com/trazolabs/harvest/v2"
	"gitlab.com/trazolabs/harvest/v2/harvestdb"
)

type Config struct {
	PostgresURL string `json:"POSTGRES_URL"`
}

type Thing struct {
	harvestdb.UUIDField
	Name string
}

type RootController struct {
}

func (c *RootController) File() *harvest.View {
	return harvest.Get("/file", func(r *harvest.Request, _ any) (any, error) {
		return &harvest.FileResponse{
			ContentType:        "text/plain",
			ContentDisposition: `attachment; filename="myfile.txt"`,
			Writer: func(writer io.Writer) error {
				_, err := writer.Write([]byte("this is the content"))
				return err
			},
		}, nil
	})
}

func (c *RootController) Redirect() *harvest.View {
	return harvest.Get("/redirect", func(r *harvest.Request, _ any) (any, error) {
		return &harvest.RedirectResponse{
			URL:        "https://example.org",
			StatusCode: http.StatusSeeOther,
		}, nil
	})
}

func (c *RootController) Plain() *harvest.View {
	return harvest.Get("/plain", func(r *harvest.Request, _ any) (any, error) {
		return &harvest.PlainResponse{
			Data: []byte("this is a plain response"),
		}, nil
	})
}

func main() {
	app := harvest.New()
	app.AddController("/root", &RootController{})
	app.Serve()
}
