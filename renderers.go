package harvest

import (
	"io"
	"net/http"
)

type Renderer interface {
	Render(v *View, w http.ResponseWriter, _ *http.Request) error
}

type PlainResponse struct {
	Data []byte
}

func (r *PlainResponse) Render(_ *View, w http.ResponseWriter, _ *http.Request) error {
	w.Header().Set("Content-Type", "text/plain")
	// TODO: is this needed?
	// w.Header().Set("Access-Control-Allow-Origin", os.Getenv("ALLOWED_ORIGIN"))
	// w.Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Secret-Token, X-Acting-As, X-Origin")
	// w.Header().Set("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS")

	w.WriteHeader(http.StatusOK)

	if _, err := w.Write(r.Data); err != nil {
		return NewError(http.StatusInternalServerError, err)
	}

	return nil
}

type ProxyResponse struct {
	Response *http.Response
}

func (r *ProxyResponse) Render(_ *View, w http.ResponseWriter, _ *http.Request) error {
	// This will be set by harvest instead.
	r.Response.Header.Del("Access-Control-Allow-Origin")
	// r.Response.Header.Del("Access-Control-Allow-Methods")

	for key, values := range r.Response.Header {
		for _, value := range values {
			w.Header().Add(key, value)
		}
	}

	w.WriteHeader(r.Response.StatusCode)
	if _, err := io.Copy(w, r.Response.Body); err != nil {
		return err
	}
	r.Response.Body.Close()
	return nil
}

type RedirectResponse struct {
	URL        string
	StatusCode int
}

func (r *RedirectResponse) Render(_ *View, w http.ResponseWriter, req *http.Request) error {
	http.Redirect(w, req, r.URL, r.StatusCode)
	return nil
}

type FileResponse struct {
	ContentType        string
	ContentDisposition string
	Writer             func(writer io.Writer) error
}

func (r *FileResponse) Render(_ *View, w http.ResponseWriter, req *http.Request) error {
	w.Header().Set("Content-Disposition", r.ContentDisposition)
	w.Header().Set("Content-Type", r.ContentType)

	if err := r.Writer(w); err != nil {
		return NewError(http.StatusInternalServerError, err)
	}

	return nil
}

type RawResponse struct {
	Handle func(w http.ResponseWriter) error
}

func (r *RawResponse) Render(_ *View, w http.ResponseWriter, req *http.Request) error {
	return r.Handle(w)
}
