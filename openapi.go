package harvest

import (
	"errors"
	"path"
	"reflect"

	"github.com/getkin/kin-openapi/openapi3"
	"github.com/getkin/kin-openapi/openapi3gen"
)

func (a *App) OpenAPISpec() ([]byte, error) {
	if a.spec == nil {
		return nil, errors.New("open api not configured")
	}

	return a.spec.MarshalJSON()
}

func (a *App) registerOpenAPI(prefix string, view *View) {
	if a.spec != nil && view.openapi.summary != "" {
		pathItem := a.spec.Paths.Find(path.Join(prefix, view.path))
		if pathItem == nil {
			pathItem = &openapi3.PathItem{}
		}

		op := openapi3.NewOperation()
		op.Summary = view.openapi.summary
		op.Description = view.openapi.description
		op.Tags = view.openapi.tags

		// Set request schema.
		inputSchemaRef, err := openapi3gen.NewSchemaRefForValue(view.openapi.input, a.spec.Components.Schemas,
			openapi3gen.UseAllExportedFields(),
			openapi3gen.SchemaCustomizer(openAPISchemaCustomizer),
		)
		if err != nil {
			panic(err)
		}

		if !inputSchemaRef.Value.IsEmpty() {
			op.RequestBody = &openapi3.RequestBodyRef{
				Value: openapi3.NewRequestBody().WithContent(map[string]*openapi3.MediaType{
					"application/json": {
						Schema: inputSchemaRef,
					},
				}),
			}
		}

		// Set response schema.
		// TODO: this does not seem to be working, FileResponse still renders.
		if _, ok := view.openapi.output.(Renderer); !ok {
			outputSchemaRef, err := openapi3gen.NewSchemaRefForValue(view.openapi.output, a.spec.Components.Schemas,
				openapi3gen.UseAllExportedFields(),
				openapi3gen.SchemaCustomizer(openAPISchemaCustomizer),
			)
			if err != nil {
				panic(err)
			}

			if !outputSchemaRef.Value.IsEmpty() {
				op.AddResponse(0, openapi3.NewResponse().
					WithContent(map[string]*openapi3.MediaType{
						"application/json": {Schema: outputSchemaRef},
					}),
				)
			}
		}

		// Add path params.
		for key, param := range view.openapi.pathParms {
			// shadow input property if repeated in path params.
			delete(inputSchemaRef.Value.Properties, key)

			pathParam := openapi3.NewPathParameter(key).
				WithDescription(param.description).
				WithSchema(openapi3.NewStringSchema())

			op.AddParameter(pathParam)
		}

		// Add query params.
		for key, param := range view.openapi.queryParams {
			// shadow input property if repeated in query params.
			delete(inputSchemaRef.Value.Properties, key)

			queryParam := openapi3.NewQueryParameter(key).
				WithDescription(param.description).
				WithSchema(openapi3.NewStringSchema()).
				WithRequired(param.required)

			op.AddParameter(queryParam)
		}

		pathItem.SetOperation(view.method, op)

		a.spec.Paths.Set(path.Join(prefix, view.path), pathItem)
	}
}

func openAPISchemaCustomizer(name string, ft reflect.Type, tag reflect.StructTag, schema *openapi3.Schema) error {
	if desc := tag.Get("desc"); desc != "" {
		schema.Description = desc
	}
	return nil
}
