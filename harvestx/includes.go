package harvestx

import (
	"log/slog"
	"reflect"
	"slices"
	"strings"

	"gitlab.com/trazolabs/harvest/v2"
	"gitlab.com/trazolabs/harvest/v2/harvestdb"
)

type Lister interface {
	List() any
}

type IncludeMiddleware struct {
	includeFuncs map[string]map[string]IncludeFunc
}

func NewIncludeMiddleware() *IncludeMiddleware {
	return &IncludeMiddleware{
		includeFuncs: make(map[string]map[string]IncludeFunc),
	}
}

func (m *IncludeMiddleware) AddIncluder(model Includer, includer any) {
	for name, fn := range m.discoverIncludeFuncs(includer) {
		modelName := canonicalModelName(model)
		m.addIncludeFunc(model, name, fn)

		slog.Debug("registering an includer",
			slog.String("model", modelName),
			slog.String("includer", name),
		)
	}
}

func IncluderPreload(r *harvest.Request, columns ...string) harvestdb.QuerierOption {
	include, err := r.Vars().String("include")
	if err != nil || include == "" {
		return nil
	}

	included := strings.Split(include, ",")

	preload := make([]string, 0)
	for _, includer := range included {
		if slices.Contains(columns, includer) {
			preload = append(preload, includer)
		}
	}

	return harvestdb.WithPreload(preload...)
}

func (m *IncludeMiddleware) Middleware() harvest.RawHandler {
	return func(r *harvest.Request) (any, error) {
		output, err := r.Next()
		if err != nil {
			return nil, err
		}

		include, err := r.Vars().String("include")
		if err != nil || include == "" {
			return output, nil
		}

		included := strings.Split(include, ",")

		if pageResponse, ok := output.(Lister); ok {
			if err := m.loadIncludes(r, pageResponse.List(), included); err != nil {
				return nil, err
			}
		}

		if err := m.loadIncludes(r, output, included); err != nil {
			return nil, err
		}

		return output, nil
	}
}

func (m *IncludeMiddleware) addIncludeFunc(model any, include string, handler IncludeFunc) {
	theModelName := canonicalModelName(model)
	if _, ok := m.includeFuncs[theModelName]; !ok {
		m.includeFuncs[theModelName] = map[string]IncludeFunc{}
	}

	m.includeFuncs[theModelName][include] = handler
}

func (m *IncludeMiddleware) discoverIncludeFuncs(includer any) map[string]IncludeFunc {
	typeOf := reflect.TypeOf(includer)
	valueOf := reflect.ValueOf(includer)

	includeFuncs := make(map[string]IncludeFunc)

	for i := 0; i < typeOf.NumMethod(); i++ {
		methodName := typeOf.Method(i).Name
		methodValue := valueOf.Method(i)
		methodType := typeOf.Method(i).Type

		if methodType.NumIn() != 1 {
			continue
		}

		if methodType.NumOut() != 1 {
			continue
		}

		if methodType.Out(0).Name() != "IncludeFunc" {
			continue
		}

		returnValues := methodValue.Call([]reflect.Value{})
		includeFunc := returnValues[0].Interface().(IncludeFunc)
		includeFuncs[methodName] = includeFunc
	}

	return includeFuncs
}

func (m *IncludeMiddleware) loadIncludes(r *harvest.Request, model any, included []string) error {
	if isSlice(model) {
		return m.loadIncludesSlice(r, model, included)
	}

	if includer, ok := model.(Includer); ok {
		found := make(map[string]any)
		methods, ok := m.includeFuncs[canonicalModelName(model)]
		if !ok {
			return nil
		}

		for _, include := range included {
			method, ok := methods[include]
			if !ok {
				continue
			}

			out, err := method(r, model)
			if err != nil {
				return err
			}

			found[include] = out
		}

		includer.SetIncluded(found)
	}

	return nil
}

func (m *IncludeMiddleware) loadIncludesSlice(r *harvest.Request, objs any, include []string) error {
	valueOf := reflect.ValueOf(objs)
	if reflect.TypeOf(objs).Kind() == reflect.Ptr {
		valueOf = valueOf.Elem()
	}

	for i := 0; i < valueOf.Len(); i++ {
		item := valueOf.Index(i).Interface()

		if err := m.loadIncludes(r, item, include); err != nil {
			return err
		}
	}

	return nil
}

func isSlice(v any) bool {
	typeOf := reflect.TypeOf(v)
	if typeOf.Kind() == reflect.Ptr {
		typeOf = typeOf.Elem()
	}

	return typeOf.Kind() == reflect.Slice
}

func canonicalModelName(m any) string {
	if reflect.TypeOf(m).Kind() == reflect.Ptr {
		m = reflect.ValueOf(m).Elem().Interface()
	}
	return reflect.TypeOf(m).Name()
}

type IncludeFunc func(r *harvest.Request, obj any) (any, error)

// Includer is the type used to determine if an instance uses the include system.
type Includer interface {
	SetIncluded(fields map[string]any)
}

// Include implements the Includer interface.
// Use as an embedded struct in models that use the include system.
type Include struct {
	Included map[string]any `gorm:"-" json:",omitempty"`
}

func (include *Include) SetIncluded(included map[string]any) {
	include.Included = included
}
