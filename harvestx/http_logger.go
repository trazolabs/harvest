package harvestx

import (
	"errors"
	"fmt"
	"log/slog"
	"net/http"
	"time"

	"gitlab.com/trazolabs/harvest/v2"
)

func HTTPLoggerMiddleware(onlyErrors bool) harvest.RawHandler {
	return func(r *harvest.Request) (any, error) {
		start := time.Now()
		output, err := r.Next()
		duration := time.Since(start) / time.Millisecond

		attrs := []any{
			slog.Int64("ms", int64(duration)),
		}

		var harvestErr *harvest.Error
		if err == nil {
			attrs = append(attrs, slog.Int("status", http.StatusOK))
		} else if errors.As(err, &harvestErr) {
			attrs = append(attrs, slog.Int("status", harvestErr.StatusCode))
			attrs = append(attrs, slog.String("error", harvestErr.Error()))

			dataGroup := make([]any, 0, len(harvestErr.Data()))
			for k, v := range harvestErr.Data() {
				dataGroup = append(dataGroup, slog.String(k, fmt.Sprintf("%v", v)))
			}
			attrs = append(attrs, slog.Group("data", dataGroup...))
		} else {
			attrs = append(attrs, slog.Int("status", http.StatusInternalServerError))
			attrs = append(attrs, slog.String("error", err.Error()))
		}

		if err != nil {
			r.Logger().Warn("http request", attrs...)
		} else if !onlyErrors {
			r.Logger().Debug("http request", attrs...)
		}

		return output, err
	}
}
