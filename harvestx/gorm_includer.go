package harvestx

import (
	"log/slog"
	"reflect"
	"strings"

	"github.com/jinzhu/gorm"
	"gitlab.com/trazolabs/harvest/v2"
	"gitlab.com/trazolabs/harvest/v2/harvestdb"
)

type GormIncluder struct {
	includeFuncs map[string]map[string]IncludeFunc
}

func NewGormIncluder() *GormIncluder {
	return &GormIncluder{
		includeFuncs: make(map[string]map[string]IncludeFunc),
	}
}

func (m *GormIncluder) AddIncluder(model Includer, includer any) {
	for name, fn := range m.discoverIncludeFuncs(includer) {
		modelName := canonicalModelName(model)
		m.addIncludeFunc(model, name, fn)

		slog.Debug("registering an includer",
			slog.String("model", modelName),
			slog.String("includer", name),
		)
	}
}

func (m *GormIncluder) Register(db *gorm.DB) {
	db.Callback().Query().After("gorm:after_query").Register("include_after_query", func(scope *gorm.Scope) {
		val, ok := scope.Get("request")
		if !ok {
			// do nothing.
			return
		}

		r := val.(*harvest.Request)
		include, err := r.Vars().String("include")
		if err != nil || include == "" {
			// do nothing.
			return
		}

		if err = m.loadIncludes(r, scope.Value, strings.Split(include, ",")); err != nil {
			db.AddError(err)
		}
	})
}

func (m *GormIncluder) addIncludeFunc(model any, include string, handler IncludeFunc) {
	theModelName := canonicalModelName(model)
	if _, ok := m.includeFuncs[theModelName]; !ok {
		m.includeFuncs[theModelName] = map[string]IncludeFunc{}
	}

	m.includeFuncs[theModelName][include] = handler
}

func (m *GormIncluder) discoverIncludeFuncs(includer any) map[string]IncludeFunc {
	typeOf := reflect.TypeOf(includer)
	valueOf := reflect.ValueOf(includer)

	includeFuncs := make(map[string]IncludeFunc)

	for i := 0; i < typeOf.NumMethod(); i++ {
		methodName := typeOf.Method(i).Name
		methodValue := valueOf.Method(i)
		methodType := typeOf.Method(i).Type

		if methodType.NumIn() != 1 {
			continue
		}

		if methodType.NumOut() != 1 {
			continue
		}

		if methodType.Out(0).Name() != "IncludeFunc" {
			continue
		}

		returnValues := methodValue.Call([]reflect.Value{})
		includeFunc := returnValues[0].Interface().(IncludeFunc)
		includeFuncs[methodName] = includeFunc
	}

	return includeFuncs
}

func (m *GormIncluder) loadIncludes(r *harvest.Request, model any, included []string) error {
	if isSlice(model) {
		return m.loadIncludesSlice(r, model, included)
	}

	if includer, ok := model.(Includer); ok {
		found := make(map[string]any)
		methods, ok := m.includeFuncs[canonicalModelName(model)]
		if !ok {
			return nil
		}

		for _, include := range included {
			method, ok := methods[include]
			if !ok {
				continue
			}

			out, err := method(r, model)
			if err != nil {
				return err
			}

			if out != nil {
				found[include] = out
			}
		}

		includer.SetIncluded(found)
	}

	return nil
}

func (m *GormIncluder) loadIncludesSlice(r *harvest.Request, objs any, include []string) error {
	valueOf := reflect.ValueOf(objs)
	if reflect.TypeOf(objs).Kind() == reflect.Ptr {
		valueOf = valueOf.Elem()
	}

	for i := 0; i < valueOf.Len(); i++ {
		item := valueOf.Index(i).Interface()

		if err := m.loadIncludes(r, item, include); err != nil {
			return err
		}
	}

	return nil
}

func MultiIncluderPreload(r *harvest.Request, name string, columns ...string) harvestdb.QuerierOption {
	include, err := r.Vars().String("include")
	if err != nil || include == "" {
		return nil
	}

	included := strings.Split(include, ",")

	preload := make([]string, 0)
	for _, includer := range included {
		if name == includer {
			preload = append(preload, columns...)
		}
	}

	return harvestdb.WithPreload(preload...)
}
