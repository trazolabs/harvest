package harvestext

import (
	"io"
	"path"

	"gitlab.com/trazolabs/harvest/v2"
)

type File struct {
	io.Reader

	extension   string
	contentType string
	size        int64
}

// NewFile creates a new file struct.
func NewFile(reader io.Reader, extension, contentType string, size int64) *File {
	return &File{
		extension:   extension,
		contentType: contentType,
		size:        size,
		Reader:      reader,
	}
}

// FileFromRequest extracts a file from a request using the given name.
// returns a nil file if there's no file by that name.
func NewFileFromRequest(r *harvest.Request, field string) (*File, error) {
	file, header, err := r.FormFile(field)
	if err != nil {
		return nil, err
	}

	return &File{
		extension:   path.Ext(header.Filename),
		contentType: header.Header.Get("Content-Type"),
		size:        header.Size,
		Reader:      file,
	}, nil
}

func (f *File) Size() int64 {
	return f.size
}

func (f *File) ContentType() string {
	return f.contentType
}

func (f *File) Extension() string {
	return f.extension
}
