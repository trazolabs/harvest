package harvestext

import (
	"fmt"
)

type StorageProvider interface {
	Store(file *File, filePath string) (*StoreResult, error)
	Delete(filePath string) error
}

type StoreResult struct {
	RelativePath string
	endpoint     string
	cdnDomain    string
}

func (r *StoreResult) StoragePath() string {
	return fmt.Sprintf("%s/%s", r.endpoint, r.RelativePath)
}

func (r *StoreResult) CDNPath() string {
	if r.cdnDomain == "" {
		return ""
	}

	return fmt.Sprintf("%s/%s", r.cdnDomain, r.RelativePath)
}
