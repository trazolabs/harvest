package harvestext

import (
	"fmt"
	"net/mail"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestPostmarkProvider_Send(t *testing.T) {
	provider := NewPostmarkProvider("< AVOID TESTING THIS >", "")

	composer := NewEmailComposer()

	composer.Template = "trazoevents-base-template"

	composer.To = mail.Address{
		Name:    "Marcelo Jara",
		Address: "gerson@trazolabs.com",
	}

	composer.From = mail.Address{
		Name:    "Apolix",
		Address: "noreply@eventap.app",
	}

	reply := mail.Address{
		Name:    "test",
		Address: "gerson+2@trazolabs.com",
	}

	fmt.Println("reply", reply.String())
	composer.AddTemplateData("HTML", "<h1>Hola!</h1>")
	composer.AddTemplateData("Subject", "A Subject")
	composer.AddTemplateData("Title", "A Title")
	composer.AddTemplateData("Congress", map[string]string{
		"Name":   "Testing Congress",
		"Slogan": "Testing Slogan",
	})

	err := provider.Send(composer)

	require.NoError(t, err)
}
