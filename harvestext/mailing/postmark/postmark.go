package postmark

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"
)

var (
	postmarkURL = `https://api.postmarkapp.com`
)

type parameters struct {
	// Method is HTTP method type.
	Method string
	// Path is postfix for URI.
	Path string
	// Payload for the request.
	Payload any
	// TokenType defines which token to use
	TokenType string
}

type TemplatedEmail struct {
	// TemplateId: REQUIRED if TemplateAlias is not specified. - The template id to use when sending this message.
	TemplateId int64 `json:",omitempty"`
	// TemplateAlias: REQUIRED if TemplateId is not specified. - The template alias to use when sending this message.
	TemplateAlias string `json:",omitempty"`
	// TemplateModel: The model to be applied to the specified template to generate HtmlBody, TextBody, and Subject.
	TemplateModel map[string]any `json:",omitempty"`
	// InlineCss: By default, if the specified template contains an HTMLBody, we will apply the style blocks as inline attributes to the rendered HTML content. You may opt-out of this behavior by passing false for this request field.
	InlineCss bool   `json:",omitempty"`
	From      string `json:",omitempty"`
	To        string `json:",omitempty"`
	Cc        string `json:",omitempty"`
	Bcc       string `json:",omitempty"`
	// Tag: Email tag that allows you to categorize outgoing emails and get detailed statistics.
	Tag string `json:",omitempty"`
	// Reply To override email address. Defaults to the Reply To set in the sender signature.
	ReplyTo string   `json:",omitempty"`
	Subject string   `json:",omitempty"`
	Headers []Header `json:",omitempty"`
	// TrackOpens: Activate open tracking for this email.
	TrackOpens  bool              `json:",omitempty"`
	Attachments []Attachment      `json:",omitempty"`
	Metadata    map[string]string `json:",omitempty"`
}

type Header struct {
	// Name: header name
	Name string
	// Value: header value
	Value string
}

type Attachment struct {
	// Name: attachment name
	Name string
	// Content: Base64 encoded attachment data
	Content string
	// ContentType: attachment MIME type
	ContentType string
	// ContentId: populate for inlining images with the images cid
	ContentID string `json:",omitempty"`
}

type EmailResponse struct {
	// To: Recipient email address
	To string
	// SubmittedAt: Timestamp
	SubmittedAt time.Time
	// MessageID: ID of message
	MessageID string
	// ErrorCode: API GetErrorMessage Codes
	ErrorCode int64
	// Message: Response message
	Message string
}

// Client provides a connection to the Postmark API
type Client struct {
	// HTTPClient is &http.Client{} by default
	HTTPClient *http.Client
	// Server Token: Used for requests that require server level privileges. This token can be found on the Credentials tab under your Postmark server.
	ServerToken string
	// AccountToken: Used for requests that require account level privileges. This token is only accessible by the account owner, and can be found on the Account tab of your Postmark account.
	AccountToken string
	// BaseURL is the root API endpoint
	BaseURL string
}

func NewClient(serverToken string, accountToken string) *Client {
	return &Client{
		HTTPClient:   &http.Client{},
		ServerToken:  serverToken,
		AccountToken: accountToken,
		BaseURL:      postmarkURL,
	}
}

func (client *Client) SendTemplatedEmail(email TemplatedEmail) (EmailResponse, error) {
	res := EmailResponse{}
	err := client.doRequest(parameters{
		Method:    "POST",
		Path:      "email/withTemplate",
		Payload:   email,
		TokenType: client.ServerToken,
	}, &res)
	return res, err
}

func (client *Client) SendTemplatedEmailBatch(emails []TemplatedEmail) ([]EmailResponse, error) {
	res := []EmailResponse{}
	var formatEmails map[string]any = map[string]any{
		"Messages": emails,
	}
	err := client.doRequest(parameters{
		Method:    "POST",
		Path:      "email/batchWithTemplates",
		Payload:   formatEmails,
		TokenType: client.ServerToken,
	}, &res)
	return res, err
}

func (client *Client) ValidateTemplate(validateTemplateBody ValidateTemplateBody) (ValidateTemplateResponse, error) {
	res := ValidateTemplateResponse{}
	err := client.doRequest(parameters{
		Method:    "POST",
		Path:      "templates/validate",
		Payload:   validateTemplateBody,
		TokenType: client.ServerToken,
	}, &res)
	return res, err
}

func (client *Client) doRequest(opts parameters, dst any) error {
	url := fmt.Sprintf("%s/%s", client.BaseURL, opts.Path)

	req, err := http.NewRequest(opts.Method, url, nil)
	if err != nil {
		return err
	}

	if opts.Payload != nil {
		payloadData, err := json.Marshal(opts.Payload)
		if err != nil {
			return err
		}
		req.Body = io.NopCloser(bytes.NewBuffer(payloadData))
	}

	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/json")

	switch opts.TokenType {
	case client.AccountToken:
		req.Header.Add("X-Postmark-Account-Token", client.AccountToken)

	default:
		req.Header.Add("X-Postmark-Server-Token", client.ServerToken)
	}

	res, err := client.HTTPClient.Do(req)
	if err != nil {
		return err
	}

	defer res.Body.Close()
	body, err := io.ReadAll(res.Body)
	if err != nil {
		return err
	}

	err = json.Unmarshal(body, dst)
	return err
}

// ResponseError represents errors returned by Postmark
type ResponseError struct {
	// ErrorCode: see error codes here (http://developer.postmarkapp.com/developer-api-overview.html#error-codes)
	ErrorCode int64
	// Message contains error details
	Message string
}

// GetErrorMessage returns the error message details
func (res ResponseError) GetErrorMessage() string {
	return res.Message
}

// ValidateTemplateResponse contains information as to how the validation went
type ValidateTemplateResponse struct {
	AllContentIsValid      bool
	HTMLBody               FieldValidationResponse `json:"HtmlBody"`
	TextBody               FieldValidationResponse
	Subject                FieldValidationResponse
	SuggestedTemplateModel map[string]any
}

// FieldValidationResponse contains the results of a field's validation
type FieldValidationResponse struct {
	ContentIsValid   bool
	ValidationErrors []ValidationError
	RenderedContent  string
}

// ValidationError contains information about the errors which occurred during validation for a given field
type ValidationError struct {
	Message           string
	Line              int
	CharacterPosition int
}

// ValidateTemplateBody contains the template/render model combination to be validated
type ValidateTemplateBody struct {
	Subject                    string
	TextBody                   string
	HTMLBody                   string `json:"HtmlBody"`
	TestRenderModel            map[string]any
	InlineCSSForHTMLTestRender bool `json:"InlineCssForHtmlTestRender"`
}
