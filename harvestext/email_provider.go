package harvestext

type EmailProvider interface {
	Send(composer *EmailComposer) error
	SendBatch(composers []*EmailComposer) ([]EmailResult, error)
}

type EmailResult struct {
	Err error
}
