package harvestext

import (
	"github.com/minio/minio-go/v6"
)

type S3Options struct {
	AccessKey string `env:"S3_ACCESS_KEY,required"`
	SecretKey string `env:"S3_SECRET_KEY,required"`
	Endpoint  string `env:"S3_ENDPOINT,required"`
}

type S3StorageProvider struct {
	client *minio.Client
	bucket string
	cdn    string
}

func NewS3StorageProvider(s3Options S3Options, bucket string, options ...S3StorageProviderOption) (*S3StorageProvider, error) {
	client, err := minio.New(s3Options.Endpoint, s3Options.AccessKey, s3Options.SecretKey, true)
	if err != nil {
		return nil, err
	}

	provider := &S3StorageProvider{client: client, bucket: bucket}

	for _, opt := range options {
		opt(provider)
	}

	return provider, nil
}

func (p *S3StorageProvider) Store(file *File, filePath string) (*StoreResult, error) {
	if _, err := p.client.PutObject(p.bucket, filePath, file.Reader, file.Size(), minio.PutObjectOptions{
		ContentType:  file.ContentType(),
		CacheControl: "no-cache",
		UserMetadata: map[string]string{"x-amz-acl": "public-read"},
	}); err != nil {
		return nil, err
	}

	endpointURL := p.client.EndpointURL()
	endpointURL.Host = p.bucket + "." + p.client.EndpointURL().Host

	return &StoreResult{
		RelativePath: filePath,
		endpoint:     endpointURL.String(),
		cdnDomain:    p.cdn,
	}, nil
}

func (p *S3StorageProvider) Delete(filePath string) error {
	return p.client.RemoveObject(p.bucket, filePath)
}

type S3StorageProviderOption func(p *S3StorageProvider)

func WithCDN(cdn string) S3StorageProviderOption {
	return func(p *S3StorageProvider) {
		p.cdn = cdn
	}
}
