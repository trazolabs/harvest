package harvestext

import (
	"encoding/base64"
	"io"
	"net/mail"
)

type EmailComposer struct {
	Template    string
	From        mail.Address
	To          mail.Address
	ReplyTo     *mail.Address
	Cc          []mail.Address
	Subject     string
	Data        map[string]any
	Attachments []EmailAttachment

	// Tag to categorize this email.
	// Enabled with Postmark.
	Tag string

	// Metadata to attach to this email, useful to debug errors like bounces.
	// Enabled with Postmark.
	Metadata map[string]string
}

type EmailAttachment struct {
	Content     string
	Filename    string
	ContentType string
}

func NewEmailComposer() *EmailComposer {
	return &EmailComposer{
		Attachments: make([]EmailAttachment, 0),
		Data:        make(map[string]any),
	}
}

func (composer *EmailComposer) AddTemplateData(key string, value any) {
	composer.Data[key] = value
}

func (composer *EmailComposer) AddAttachmentReader(filename, contentType string, reader io.Reader) error {
	content, err := io.ReadAll(reader)
	if err != nil {
		return err
	}

	encoded := base64.StdEncoding.EncodeToString(content)

	composer.Attachments = append(composer.Attachments, EmailAttachment{
		Filename:    filename,
		Content:     encoded,
		ContentType: contentType,
	})

	return nil
}
