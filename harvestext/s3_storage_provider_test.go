package harvestext

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestS3StorageProvider_Store(t *testing.T) {
	imageReader, err := os.Open("s3_storage_provider.go")
	require.NoError(t, err)
	stat, err := imageReader.Stat()
	require.NoError(t, err)
	file := NewFile(imageReader, ".go", "text/plain", stat.Size())

	p, err := NewS3StorageProvider(S3Options{
		AccessKey: "SGGBHXVFOACVVGZMBUBR",
		SecretKey: "K7NsX41T759XW8GSRMx2IzsL9/DIpSG5M4eQ/n4Ce3M",
		Endpoint:  "sfo2.digitaloceanspaces.com",
	}, "trazolabs-dev", WithCDN("https://cdn.trazolabs.com"))
	require.NoError(t, err)

	result, err := p.Store(file, "testing/s3_storage_provider.go")
	require.NoError(t, err)

	t.Log("RelativePath", result.RelativePath)
	t.Log("StoragePath", result.StoragePath())
	t.Log("CDNPath", result.CDNPath())
}

func TestS3StorageProvider_Delete(t *testing.T) {
	p, err := NewS3StorageProvider(S3Options{
		AccessKey: "SGGBHXVFOACVVGZMBUBR",
		SecretKey: "K7NsX41T759XW8GSRMx2IzsL9/DIpSG5M4eQ/n4Ce3M",
		Endpoint:  "sfo2.digitaloceanspaces.com",
	}, "trazolabs-dev")
	require.NoError(t, err)

	err = p.Delete("testing/s3_storage_provider.go")
	require.NoError(t, err)
}
