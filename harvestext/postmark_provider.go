package harvestext

import (
	"fmt"
	"math"

	"gitlab.com/trazolabs/harvest/v2/harvestext/mailing/postmark"
)

type PostmarkProvider struct {
	serverToken  string
	accountToken string
}

func NewPostmarkProvider(serverToken string, accountToken string) *PostmarkProvider {
	return &PostmarkProvider{serverToken: serverToken, accountToken: accountToken}
}

func (p *PostmarkProvider) Send(composer *EmailComposer) error {
	client := postmark.NewClient(p.serverToken, p.accountToken)

	response, err := client.SendTemplatedEmail(p.buildTemplatedEmail(composer))
	if err != nil {
		return err
	}

	if response.ErrorCode != 0 {
		return fmt.Errorf("postmark error: %s (%d)", response.Message, response.ErrorCode)
	}

	return nil
}

func (p *PostmarkProvider) SendBatch(composers []*EmailComposer) ([]EmailResult, error) {
	client := postmark.NewClient(p.serverToken, p.accountToken)
	emails := make([]postmark.TemplatedEmail, len(composers))
	results := make([]EmailResult, 0)

	for i, composer := range composers {
		emails[i] = p.buildTemplatedEmail(composer)
	}

	// the postmark API endpoint accepts batches of 500.
	for i := 0; i < len(emails); i += 500 {
		batch := emails[i:int(math.Min(float64(i+500), float64(len(emails))))]

		responses, err := client.SendTemplatedEmailBatch(batch)
		if err != nil {
			return nil, err
		}

		for _, response := range responses {
			if response.ErrorCode != 0 {
				results = append(results, EmailResult{
					Err: fmt.Errorf("%s (%d)", response.Message, response.ErrorCode),
				})
			}
		}
	}

	return results, nil
}

func (p *PostmarkProvider) buildTemplatedEmail(composer *EmailComposer) postmark.TemplatedEmail {
	email := postmark.TemplatedEmail{
		TemplateAlias: composer.Template,
		TemplateModel: composer.Data,
		From:          composer.From.String(),
		To:            composer.To.String(),
		Subject:       composer.Subject,
		TrackOpens:    true,
		Tag:           composer.Tag,
		Metadata:      composer.Metadata,
	}

	if composer.ReplyTo != nil {
		email.ReplyTo = composer.ReplyTo.String()
	}

	if len(composer.Cc) > 0 {
		CcString := ""
		for i, reply := range composer.Cc {
			if i < (len(composer.Cc) - 1) {
				CcString += reply.String() + ","
			} else {
				CcString += reply.String()
			}
		}
		email.Cc = CcString
	}

	for _, attachment := range composer.Attachments {
		email.Attachments = append(email.Attachments, postmark.Attachment{
			Name:        attachment.Filename,
			Content:     attachment.Content,
			ContentType: attachment.ContentType,
		})
	}

	return email
}
