package harvest

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/getkin/kin-openapi/openapi3"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

var (
	defaultAddress        = ":5050"
	defaultAllowedOrigins = []string{"*"}
	defaultAllowedMethods = []string{"GET", "HEAD", "POST", "PUT", "OPTIONS", "DELETE"}
	defaultAllowedHeaders = []string{
		"Origin", "X-Requested-With", "Content-Type", "Accept", "Authorization",
		"Secret-Token", "X-Origin", "X-Acting-As",
	}
)

type App struct {
	middlewares []RawHandler
	router      *mux.Router
	logger      *slog.Logger
	db          *gorm.DB

	// options.
	allowedOrigins []string
	allowedMethods []string
	allowedHeaders []string
	handlerWrapper func(http.Handler) http.Handler
	address        string
	spec           *openapi3.T
}

func New(options ...ApplicationOption) *App {
	app := &App{
		router:         mux.NewRouter(),
		middlewares:    make([]RawHandler, 0),
		address:        defaultAddress,
		allowedMethods: defaultAllowedMethods,
		allowedHeaders: defaultAllowedHeaders,
		allowedOrigins: defaultAllowedOrigins,
		logger:         slog.New(slog.NewJSONHandler(os.Stderr, defaultLoggerHandlerOptions())),
		handlerWrapper: func(h http.Handler) http.Handler { return h },
	}

	for _, option := range options {
		option(app)
	}

	return app
}

type ApplicationOption func(app *App)

// WithLogger replaces the default application logger.
func WithLogger(logger *slog.Logger) ApplicationOption {
	return func(app *App) {
		app.logger = logger
	}
}

// WithDatabase to set up this application with a database.
func WithDatabase(db *gorm.DB) ApplicationOption {
	return func(app *App) {
		app.db = db
	}
}

// WithAllowedOrigins sets the allowed origins for CORS.
func WithAllowedOrigins(allowedOrigins ...string) ApplicationOption {
	return func(app *App) {
		app.allowedOrigins = allowedOrigins
	}
}

// WithAllowedMethods sets the allowed methods for CORS.
func WithAllowedMethods(allowedMethods ...string) ApplicationOption {
	return func(app *App) {
		app.allowedMethods = allowedMethods
	}
}

// WithAllowedHeaders sets the allowed headers for CORS.
func WithAllowedHeaders(allowedHeaders ...string) ApplicationOption {
	return func(app *App) {
		app.allowedHeaders = allowedHeaders
	}
}

func WithHandlerWrapper(wrapper func(http.Handler) http.Handler) ApplicationOption {
	return func(app *App) {
		app.handlerWrapper = wrapper
	}
}

type OpenAPIInfo struct {
	Title   string
	Version string
	Tags    []OpenAPIInfoTag
}

type OpenAPIInfoTag struct {
	Name     string
	Markdown string
}

// WithOpenAPI enabled OpenAPI support for this API.
func WithOpenAPI(info OpenAPIInfo) ApplicationOption {
	return func(app *App) {
		app.spec = &openapi3.T{
			OpenAPI: "3.0.0",
			Info: &openapi3.Info{
				Title:      info.Title,
				Version:    info.Version,
				Extensions: map[string]interface{}{},
			},
			Components: &openapi3.Components{
				Schemas:    make(openapi3.Schemas),
				Extensions: map[string]interface{}{},
			},
			Paths:      &openapi3.Paths{},
			Extensions: map[string]interface{}{},
			Tags:       openapi3.Tags{},
		}

		for _, tag := range info.Tags {
			app.spec.Tags = append(app.spec.Tags, &openapi3.Tag{
				Name:        tag.Name,
				Description: tag.Markdown,
			})
		}
	}
}

type LoggerMode string

const (
	LoggerModeText LoggerMode = "TEXT"
	LoggerModeJSON LoggerMode = "JSON"
)

// WithPlainLogger sets default attributes for the application logger.
func WithLoggerMode(mode LoggerMode) ApplicationOption {
	return func(app *App) {
		if mode == LoggerModeText {
			app.logger = slog.New(slog.NewTextHandler(os.Stderr, defaultLoggerHandlerOptions()))
		} else {
			app.logger = slog.New(slog.NewJSONHandler(os.Stderr, defaultLoggerHandlerOptions()))
		}
	}
}

// WithLogAttrs sets default attributes for the application logger.
func WithLogAttrs(attrs ...any) ApplicationOption {
	return func(a *App) {
		a.logger = a.logger.With(attrs...)
	}
}

// AddController to this application. A controller is any struct with methods that
// return a *harvest.View (called view factory method).
// This method will execute all view factory methods, and add each view to the router.
func (a *App) AddController(prefix string, controller any) {
	subRouter := a.router.PathPrefix(prefix).Subrouter()

	for _, view := range discoverViews(controller) {
		a.logger.Debug("view registered",
			slog.String("path", fmt.Sprintf("%s%s", prefix, view.path)),
			slog.String("method", view.method),
		)

		view.setup(a, subRouter)
		a.registerOpenAPI(prefix, view)
	}
}

func (a *App) AddMiddleware(fn RawHandler) {
	a.middlewares = append(a.middlewares, fn)
}

func (a *App) Router() *mux.Router {
	return a.router
}

func (a *App) Logger() *slog.Logger {
	return a.logger
}

func (a *App) Serve() {
	headersOk := handlers.AllowedHeaders(a.allowedHeaders)
	originsOk := handlers.AllowedOrigins(a.allowedOrigins)
	methodsOk := handlers.AllowedMethods(a.allowedMethods)

	a.logger.Debug("http server started",
		slog.String("addr", a.address),
	)

	err := http.ListenAndServe(a.address, a.handlerWrapper(handlers.CORS(originsOk, headersOk, methodsOk)(a.router)))
	if err != nil {
		a.logger.Error("error in http server",
			slog.String("error", err.Error()),
		)
		os.Exit(1)
	}
}

func (a *App) GracefulServe() {
	headersOk := handlers.AllowedHeaders(a.allowedHeaders)
	originsOk := handlers.AllowedOrigins(a.allowedOrigins)
	methodsOk := handlers.AllowedMethods(a.allowedMethods)
	srv := &http.Server{Addr: a.address, Handler: a.handlerWrapper(handlers.CORS(originsOk, headersOk, methodsOk)(a.router))}

	idleConnsClosed := make(chan struct{})
	go func() {
		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, os.Interrupt)
		<-sigint

		ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
		defer cancel()

		// We received an interrupt signal, shut down.
		if err := srv.Shutdown(ctx); err != nil {
			a.logger.Error("error in http server shutdown",
				slog.String("error", err.Error()),
			)
		}

		close(idleConnsClosed)
	}()

	a.logger.Debug("http server started",
		slog.String("addr", a.address),
	)

	if err := srv.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
		a.logger.Error("error in http server",
			slog.String("error", err.Error()),
		)
		os.Exit(1)
	}

	<-idleConnsClosed

	a.logger.Debug("http server gracefully shut down")
}

func defaultLoggerHandlerOptions() *slog.HandlerOptions {
	return &slog.HandlerOptions{
		Level:     slog.LevelDebug,
		AddSource: true,
	}
}
