package harvest

import (
	"encoding/json"
	"errors"
	"log/slog"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

type RawHandler func(r *Request) (any, error)

type Handler[I, O any] func(r *Request, input I) (O, error)

type View struct {
	app     *App
	handler RawHandler
	method  string
	path    string

	openapi struct {
		tags        []string
		input       any
		output      any
		summary     string
		description string
		queryParams map[string]openAPIParam
		pathParms   map[string]openAPIParam
	}

	// Options.
	useTransaction bool
}

type openAPIParam struct {
	description string
	required    bool
}

func RawHandle[O any](method, path string, handler RawHandler, options ...ViewOption) *View {
	view := &View{
		method:  method,
		path:    path,
		handler: handler,

		// default options.
		useTransaction: true,
	}

	view.openapi.input = struct{}{}
	view.openapi.output = new(O)
	view.openapi.pathParms = make(map[string]openAPIParam)
	view.openapi.queryParams = make(map[string]openAPIParam)

	for _, option := range options {
		option(view)
	}

	return view
}

func Handle[I, O any](method, path string, handler Handler[I, O], options ...ViewOption) *View {
	view := RawHandle[O](method, path, wrapGeneric(handler), options...)
	view.openapi.input = new(I)
	return view
}

type ViewOption func(view *View)

// WithTransaction modifies the behavior of transactions for a view.
func WithTransaction(useTransaction bool) ViewOption {
	return func(view *View) {
		view.useTransaction = useTransaction
	}
}

// WithDescription sets the description for OpenAPI spec.
func WithDescription(description string) ViewOption {
	return func(view *View) {
		view.openapi.description = description
	}
}

// WithSummary sets the summary for OpenAPI spec.
// This is the minimum required setting to register this view
// in OpenAPI.
func WithSummary(summary string) ViewOption {
	return func(view *View) {
		view.openapi.summary = summary
	}
}

// WithTags sets the tags for OpenAPI
func WithTags(tags ...string) ViewOption {
	return func(view *View) {
		view.openapi.tags = tags
	}
}

// WithPathParam adds a path param for OpenAPI
func WithPathParam(name, description string) ViewOption {
	return func(view *View) {
		view.openapi.pathParms[name] = openAPIParam{
			description: description,
			required:    true,
		}
	}
}

// WithQueryParam adds a query param for OpenAPI
func WithQueryParam(name, description string, required bool) ViewOption {
	return func(view *View) {
		view.openapi.queryParams[name] = openAPIParam{
			description: description,
			required:    required,
		}
	}
}

// WithResponse replaces the response got from generics.
func WithResponse(response any) ViewOption {
	return func(view *View) {
		view.openapi.output = response
	}
}

func wrapGeneric[I, O any](handler Handler[I, O]) RawHandler {
	return func(r *Request) (any, error) {
		input := new(I)
		if err := r.Bind(input); err != nil {
			return nil, err
		}

		return handler(r, *input)
	}
}

func Get[I, O any](path string, handler Handler[I, O], options ...ViewOption) *View {
	return Handle("GET", path, handler, options...)
}

func Post[I, O any](path string, handler Handler[I, O], options ...ViewOption) *View {
	return Handle("POST", path, handler, options...)
}

func Put[I, O any](path string, handler Handler[I, O], options ...ViewOption) *View {
	return Handle("PUT", path, handler, options...)
}

func Delete[I, O any](path string, handler Handler[I, O], options ...ViewOption) *View {
	return Handle("DELETE", path, handler, options...)
}

func (v *View) setup(app *App, router *mux.Router) {
	v.app = app

	// setup the router handler.
	router.Methods(v.method).Path(v.path).Handler(v)
}

func (v *View) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	payload, err := v.withTransaction(wrapRequest(r, v))
	if err != nil {
		v.serveError(w, err)
		return
	}

	if renderer, ok := payload.(Renderer); ok {
		if err := renderer.Render(v, w, r); err != nil {
			v.serveError(w, err)
		}
	} else if payload != nil {
		if err := v.serveJSON(w, &JSONResponse{Data: payload}, 200); err != nil {
			v.serveError(w, err)
		}
	}
}

func (v *View) withTransaction(r *Request) (any, error) {
	var (
		response any
		err      error
	)

	if v.app.db == nil {
		response, err = r.Next()
	} else if v.useTransaction {
		err = r.db.Set("request", r).Transaction(func(tx *gorm.DB) error {
			r.db = tx
			response, err = r.Next()
			return err
		})
	} else {
		r.db = r.db.Set("request", r)
		response, err = r.Next()
	}

	return response, err
}

func (v *View) serveError(w http.ResponseWriter, err error) {
	response := &JSONResponse{}

	// Default error code.
	statusCode := http.StatusInternalServerError

	// Log error in the console for further inspection.
	var harvestErr *Error
	if errors.As(err, &harvestErr) {
		// Get real status code.
		statusCode = harvestErr.StatusCode
		response.Error = harvestErr.toResponse()
	} else {
		response.Error = httpResponseError{
			StatusCode: statusCode,
			Message:    err.Error(),
		}
	}

	if err := v.serveJSON(w, response, statusCode); err != nil {
		// Nothing else can be done.
		v.app.logger.Error("error while serving error",
			slog.String("error", err.Error()),
		)
	}
}

type JSONResponse struct {
	Data  any `json:",omitempty"`
	Error any `json:",omitempty"`
}

func (v *View) serveJSON(w http.ResponseWriter, object any, statusCode int) error {
	w.Header().Set("Content-Type", "application/json")
	// TODO: is this needed?
	// w.Header().Set("Access-Control-Allow-Origin", os.Getenv("ALLOWED_ORIGIN"))
	// w.Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Secret-Token, X-Acting-As, X-Origin")
	// w.Header().Set("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS")

	w.WriteHeader(statusCode)

	if err := json.NewEncoder(w).Encode(object); err != nil {
		return NewError(http.StatusInternalServerError, err)
	}

	return nil
}
