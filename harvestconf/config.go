package harvestconf

import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/Shopify/ejson"
	"github.com/joeshaw/envdecode"
)

type envConfig struct {
	SecretsFilePath   string `env:"SECRETS_FILE_PATH"`
	SecretsPrivateKey string `env:"SECRETS_PRIVATE_KEY"`
}

func Read[T any]() *T {
	selfConfig := &envConfig{}
	appConfig := new(T)

	if err := envdecode.Decode(selfConfig); err != nil && !errors.Is(err, envdecode.ErrNoTargetFieldsAreSet) {
		panic(fmt.Errorf("error reading harvest env configuration: %w", err))
	}

	if err := envdecode.StrictDecode(appConfig); err != nil && !errors.Is(err, envdecode.ErrInvalidTarget) {
		panic(fmt.Errorf("error reading env configuration: %w", err))
	}

	if err := readSecrets(appConfig, selfConfig); err != nil {
		panic(fmt.Errorf("error reading secrets configuration: %w", err))
	}

	return appConfig
}

func readSecrets[T any](config T, selfConfig *envConfig) error {
	// do not read any secret if not defined.
	if selfConfig.SecretsFilePath == "" && selfConfig.SecretsPrivateKey == "" {
		return nil
	}

	data, err := ejson.DecryptFile(selfConfig.SecretsFilePath, "", selfConfig.SecretsPrivateKey)
	if err != nil {
		return err
	}

	return json.Unmarshal(data, config)
}
