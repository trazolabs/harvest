package harvestdb

import (
	"fmt"
	"regexp"
	"strings"

	"gitlab.com/trazolabs/harvest/v2"
)

var orderRegex = regexp.MustCompile("^-?[A-Z][a-zA-Z0-9_]*$")

type Ordering struct {
	Column    string
	Direction string
}

func (o Ordering) String() string {
	return fmt.Sprintf(`"%s" %s`, o.Column, o.Direction)
}

func getOrderFromRequest(r *harvest.Request) ([]Ordering, error) {
	query, err := r.Vars().String("order")
	if err != nil {
		return nil, nil
	}

	orderings := make([]Ordering, 0)

	for _, order := range strings.Split(query, ",") {
		ordering, err := getOrderFromString(strings.TrimSpace(order))
		if err != nil {
			return nil, err
		}

		orderings = append(orderings, *ordering)
	}

	return orderings, nil
}

func getOrderFromString(order string) (*Ordering, error) {
	if !orderRegex.MatchString(order) {
		return nil, fmt.Errorf("invalid order query, must match (^-?[A-Z][a-zA-Z0-9_]*$)")
	}

	direction := "asc"
	column := order
	if strings.HasPrefix(order, "-") {
		direction = "desc"
		column = order[1:]
	}

	return &Ordering{
		Column:    column,
		Direction: direction,
	}, nil
}
