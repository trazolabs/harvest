package harvestdb

import (
	"fmt"
	"net/http"
	"reflect"

	"gitlab.com/trazolabs/harvest/v2"
)

// W should be used for Where mappings. As an example, use with GenericDetail(r, W{"ID": 1}, ...)
// it is just a syntax helper, instead of writing: map[string]any{ ... }, write W{ ... }
type W map[string]any

func Detail[T any](r *harvest.Request, where W, options ...QuerierOption) (*T, error) {
	config, err := newDetailConfig(r)
	if err != nil {
		return nil, err
	}

	if err := config.options(options...); err != nil {
		return nil, err
	}

	db := r.DB()

	// handle unscoped config.
	if config.unscoped {
		db = db.Unscoped()
	}

	// handle preload config.
	for _, column := range config.preload {
		db = db.Preload(column)
	}

	for attribute, value := range where {
		db = db.Where(fmt.Sprintf(`"%s" = ?`, attribute), value)
	}

	// handle apply config.
	db = config.apply(db)

	item := new(T)
	if err := db.Take(item).Error; err != nil {
		return nil, harvest.NewErrorf(http.StatusNotFound, "error fetching %s: %w", reflect.TypeOf(item).Elem().Name(), err)
	}

	return item, nil
}
