package harvestdb

import (
	"fmt"
	"net/http"
	"regexp"
	"strings"
	"unicode"

	"github.com/jinzhu/gorm"
	"gitlab.com/trazolabs/harvest/v2"
)

type F map[string][]any

func (f F) removeUnexported() {
	for key := range f {
		if !unicode.IsUpper(rune(key[0])) {
			delete(f, key)
		}
	}
}

var baseFilterOps = map[string]FilterOp{
	"equal":    Equal,
	"not":      Not,
	"ge":       GreaterEqual,
	"gt":       GreaterThan,
	"le":       LessEqual,
	"lt":       LessThan,
	"like":     Like,
	"ilike":    ILike,
	"search":   Search,
	"contains": Contains,
}

func PagedFilter[T any](r *harvest.Request, filters F, options ...QuerierOption) (*harvest.PageResponse[*T], error) {
	config, err := newFilterConfig(r)
	if err != nil {
		return nil, err
	}

	if err := config.options(options...); err != nil {
		return nil, err
	}

	items := make([]*T, 0)
	db, err := where(r.DB().Model(&items), filters, config.ops)
	if err != nil {
		return nil, err
	}

	// handle unscoped config.
	if config.unscoped {
		db = db.Unscoped()
	}

	// handle preload config.
	for _, column := range config.preload {
		db = db.Preload(column)
	}

	// handle apply config.
	db = config.apply(db)

	// handle orderings config.
	if config.orders != nil {
		for _, order := range config.orders {
			db = db.Order(order.String())
		}
	}

	page := harvest.NewPageFromRequest(r)
	if page == nil {
		return nil, harvest.NewError(http.StatusBadRequest, fmt.Errorf("must include pagination params: limit and offset"))
	}

	count := 0
	if err := db.Count(&count).Error; err != nil {
		return nil, harvest.NewError(http.StatusInternalServerError, err)
	}

	db = db.Offset(page.Offset).Limit(page.Limit)
	if err := db.Find(&items).Error; err != nil {
		return nil, harvest.NewError(http.StatusInternalServerError, err)
	}

	return &harvest.PageResponse[*T]{
		Count: count,
		Items: items,
	}, nil
}

func Filter[T any](r *harvest.Request, filters F, options ...QuerierOption) ([]*T, error) {
	config, err := newFilterConfig(r)
	if err != nil {
		return nil, err
	}

	if err := config.options(options...); err != nil {
		return nil, err
	}

	items := make([]*T, 0)
	db, err := where(r.DB().Model(&items), filters, config.ops)
	if err != nil {
		return nil, err
	}

	// handle unscoped config.
	if config.unscoped {
		db = db.Unscoped()
	}

	// handle preload config.
	for _, column := range config.preload {
		db = db.Preload(column)
	}

	// handle apply config.
	db = config.apply(db)

	// handle orderings config.
	if config.orders != nil {
		for _, order := range config.orders {
			db = db.Order(order.String())
		}
	}

	if err := db.Find(&items).Error; err != nil {
		return nil, harvest.NewError(http.StatusInternalServerError, err)
	}

	return items, nil
}

func where(db *gorm.DB, filters F, customOps map[string]FilterOp) (*gorm.DB, error) {
	filters.removeUnexported()

	for s, values := range filters {
		filter, err := NewURLFilter(s)
		if err != nil {
			return nil, err
		}

		db = applyOp(db, customOps, filter.Operator, filter.buildSelector(), values)
	}

	return db, nil
}

func applyOp(db *gorm.DB, customOps map[string]FilterOp, operator, selector string, values []any) *gorm.DB {
	if len(values) > 1 {
		return db.Where(fmt.Sprintf(`%s IN (?)`, selector), values)
	}

	if customOp, ok := customOps[operator]; ok {
		return customOp(db, selector, values[0])
	}

	op, ok := baseFilterOps[operator]
	if !ok {
		return Equal(db, selector, values[0])
	}

	return op(db, selector, values[0])
}

type urlField struct {
	Field    string
	Subfield string
}

var columnRegex = regexp.MustCompile("^[a-zA-Z_][a-zA-Z0-9_]*$")

func (f urlField) isValid() bool {
	if !columnRegex.MatchString(f.Field) {
		return false
	}

	if f.Subfield != "" && !columnRegex.MatchString(f.Field) {
		return false
	}

	return true
}

type URLFilter struct {
	Fields   []urlField
	Operator string
}

func (f *URLFilter) buildSelector() string {
	selectors := make([]string, 0)
	for _, field := range f.Fields {
		var selector string
		if field.Subfield != "" {
			selector = fmt.Sprintf(`("%s"->>'%s')`, field.Field, field.Subfield)
		} else {
			selector = fmt.Sprintf(`"%s"`, field.Field)
		}

		selectors = append(selectors, selector)
	}

	return strings.Join(selectors, "||' '||")
}

func NewURLFilter(s string) (*URLFilter, error) {
	filter := &URLFilter{}

	var fields []string
	if strings.Contains(s, "~") {
		// with operator.
		parts := strings.Split(s, "~")
		fields = strings.Split(parts[0], ",")
		filter.Operator = parts[1]
	} else {
		// without operator.
		fields = strings.Split(s, ",")
	}

	for _, field := range fields {
		theField := urlField{}

		if strings.Contains(field, ".") {
			parts := strings.Split(field, ".")
			theField.Field = parts[0]

			// check if the filter has a subfield.
			if len(parts) > 1 {
				theField.Subfield = parts[1]
			}
		} else if strings.Contains(field, "-") {
			parts := strings.Split(field, "-")
			theField.Field = parts[0]

			// check if the filter has a subfield.
			if len(parts) > 1 {
				theField.Subfield = parts[1]
			}
		} else {
			theField.Field = field
		}

		if !theField.isValid() {
			return nil, harvest.NewErrorf(http.StatusBadRequest, "query contains invalid column names")
		}

		filter.Fields = append(filter.Fields, theField)
	}

	return filter, nil
}
