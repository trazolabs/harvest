package harvestdb

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

func Create(args string, options ...Option) *gorm.DB {
	db := createDatabase(args)

	// patch gorm callback.
	gorm.DefaultCallback.Create().Replace("gorm:force_reload_after_create", forceReloadAfterCreateCallback)

	gorm.AddNamingStrategy(&gorm.NamingStrategy{
		Column: func(name string) string {
			return name
		},
		Table: func(name string) string {
			return name
		},
	})

	for _, option := range options {
		option(db)
	}

	return db
}

type Option func(db *gorm.DB)

func WithMaxOpenConnections(maxOpenConnections int) Option {
	return func(db *gorm.DB) {
		db.DB().SetMaxOpenConns(maxOpenConnections)
	}
}

func WithLogMode(logMode bool) Option {
	return func(db *gorm.DB) {
		db.LogMode(logMode)
	}
}

func WithModels(models ...any) Option {
	return func(db *gorm.DB) {
		addModels(db, models...)
	}
}

func createDatabase(args string) *gorm.DB {
	db, err := gorm.Open("postgres", args)
	if err != nil {
		panic(fmt.Errorf("failed to connect database: %w", err))
	}

	db.Exec(`CREATE EXTENSION IF NOT EXISTS "pg_trgm"`)
	db.Exec(`CREATE EXTENSION IF NOT EXISTS "uuid-ossp"`)
	db.Exec(`CREATE EXTENSION IF NOT EXISTS "unaccent"`)

	return db
}

func forceReloadAfterCreateCallback(scope *gorm.Scope) {
	if blankColumnsWithDefaultValue, ok := scope.InstanceGet("gorm:blank_columns_with_default_value"); ok {
		db := scope.DB().New().Table(scope.TableName()).Select(blankColumnsWithDefaultValue.([]string))
		for _, field := range scope.Fields() {
			if field.IsPrimaryKey && !field.IsBlank {
				db = db.Where(fmt.Sprintf("%v = ?", scope.Quote(field.DBName)), field.Field.Interface())
			}
		}
		db.Scan(scope.Value)
	}
}
