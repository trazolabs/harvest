package harvestdb

import (
	"errors"
	"fmt"
	"reflect"
	"strings"

	"github.com/jinzhu/gorm"
)

func addModels(db *gorm.DB, models ...any) {
	foreignKeys := make([]ForeignKey, 0)

	for _, model := range models {
		fks, err := extractForeignKeys(model)
		if err != nil {
			panic(fmt.Errorf("error extracting foreign keys: %w", err))
		}

		foreignKeys = append(foreignKeys, fks...)

		db.AutoMigrate(model)
	}

	for _, fk := range foreignKeys {
		db.Model(fk.Model).AddForeignKey(
			fk.FieldName,
			fmt.Sprintf(`"%s"("%s")`, fk.TableName, fk.ForeignName),
			fk.OnDelete,
			fk.OnUpdate,
		)
	}
}

type ForeignKey struct {
	Model       any
	FieldName   string
	ForeignName string
	TableName   string
	OnDelete    string
	OnUpdate    string
}

func extractForeignKeys(model any) ([]ForeignKey, error) {
	foreignKeys := make([]ForeignKey, 0)

	t := reflect.TypeOf(model)
	if reflect.TypeOf(model).Kind() == reflect.Ptr {
		t = t.Elem()
	}

	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)
		tag := field.Tag.Get("fk")

		// ignore this field if the tag is empty.
		if tag == "" {
			continue
		}

		parts := strings.Split(tag, ",")
		if len(parts) != 4 {
			return nil, errors.New("tag fk must have 4 parts separated by comma")
		}

		foreignKeys = append(foreignKeys, ForeignKey{
			Model:       model,
			FieldName:   field.Name,
			TableName:   parts[0],
			ForeignName: parts[1],
			OnDelete:    parts[2],
			OnUpdate:    parts[3],
		})
	}

	return foreignKeys, nil
}
