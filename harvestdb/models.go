package harvestdb

import (
	"time"

	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
)

type Model struct {
	CreatedAt time.Time
	UpdatedAt time.Time
}

type DeletedAtField struct {
	DeletedAt *time.Time `sql:"index"`
}

type UUIDField struct {
	UUID uuid.UUID `gorm:"type:uuid;primary_key"`
}

func (field *UUIDField) BeforeCreate(scope *gorm.Scope) error {
	field.UUID = uuid.NewV4()
	return nil
}
