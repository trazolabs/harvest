package harvestdb

import (
	"fmt"
	"strings"

	"github.com/jinzhu/gorm"
)

type FilterOp func(db *gorm.DB, selector string, value any) *gorm.DB

func Equal(db *gorm.DB, selector string, value any) *gorm.DB {
	if value == nil {
		return db.Where(fmt.Sprintf(`%s IS NULL`, selector))
	}
	return db.Where(fmt.Sprintf(`%s = ?`, selector), value)
}

func Not(db *gorm.DB, selector string, value any) *gorm.DB {
	if value == nil {
		return db.Where(fmt.Sprintf(`%s IS NOT NULL`, selector))
	}
	return db.Where(fmt.Sprintf(`%s <> ?`, selector), value)
}

func GreaterEqual(db *gorm.DB, selector string, value any) *gorm.DB {
	return db.Where(fmt.Sprintf(`%s >= ?`, selector), value)
}

func GreaterThan(db *gorm.DB, selector string, value any) *gorm.DB {
	return db.Where(fmt.Sprintf(`%s > ?`, selector), value)
}

func LessEqual(db *gorm.DB, selector string, value any) *gorm.DB {
	return db.Where(fmt.Sprintf(`%s <= ?`, selector), value)
}

func LessThan(db *gorm.DB, selector string, value any) *gorm.DB {
	return db.Where(fmt.Sprintf(`%s < ?`, selector), value)
}

func Like(db *gorm.DB, selector string, value any) *gorm.DB {
	return db.Where(fmt.Sprintf(`%s LIKE ?`, selector), fmt.Sprintf("%%%s%%", value))
}

func ILike(db *gorm.DB, selector string, value any) *gorm.DB {
	return db.Where(fmt.Sprintf(`%s ILIKE ?`, selector), fmt.Sprintf("%%%s%%", value))
}

func Search(db *gorm.DB, selector string, value any) *gorm.DB {
	valueStr := strings.Join(strings.Split(fmt.Sprint(value), " "), " & ")
	db = db.Select(fmt.Sprintf(`*, ts_rank_cd(to_tsvector(unaccent(%s)), to_tsquery(unaccent(?))) as "Rank"`, selector), valueStr)
	db = db.Where(fmt.Sprintf(`to_tsvector(unaccent(%s)) @@ to_tsquery(unaccent(?))`, selector), valueStr)
	return db.Order(gorm.Expr(fmt.Sprintf(`ts_rank_cd(to_tsvector(unaccent(%s)), to_tsquery(unaccent(?))) DESC`, selector), valueStr))
}

func Contains(db *gorm.DB, selector string, value any) *gorm.DB {
	return db.Where(fmt.Sprintf(`%s @> ?`, selector), fmt.Sprintf(`"%s"`, value))
}
