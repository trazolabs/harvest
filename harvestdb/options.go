package harvestdb

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/trazolabs/harvest/v2"
)

type ApplyFunc func(db *gorm.DB) *gorm.DB

type config struct {
	apply    ApplyFunc
	unscoped bool
	preload  []string
}

func newConfig(*harvest.Request) *config {
	return &config{
		apply:    func(db *gorm.DB) *gorm.DB { return db },
		unscoped: false,
	}
}

type filterConfig struct {
	config

	orders []Ordering
	ops    map[string]FilterOp
}

func newFilterConfig(r *harvest.Request) (*filterConfig, error) {
	orders, err := getOrderFromRequest(r)
	if err != nil {
		return nil, err
	}

	return &filterConfig{
		config: *newConfig(r),
		orders: orders,
		ops:    make(map[string]FilterOp),
	}, nil
}

func (c *filterConfig) options(options ...QuerierOption) error {
	for _, o := range options {
		if o == nil {
			continue
		}

		if err := o.filter(c); err != nil {
			return err
		}
	}
	return nil
}

type detailConfig struct {
	config
}

func newDetailConfig(r *harvest.Request) (*detailConfig, error) {
	return &detailConfig{
		config: *newConfig(r),
	}, nil
}

func (c *detailConfig) options(options ...QuerierOption) error {
	for _, o := range options {
		if o == nil {
			continue
		}

		if err := o.detail(c); err != nil {
			return err
		}
	}
	return nil
}

type QuerierOption interface {
	filter(config *filterConfig) error
	detail(config *detailConfig) error
}

type apply struct {
	fn ApplyFunc
}

func (a *apply) filter(config *filterConfig) error {
	config.apply = a.fn
	return nil
}

func (a *apply) detail(config *detailConfig) error {
	config.apply = a.fn
	return nil
}

func WithApply(fn func(db *gorm.DB) *gorm.DB) QuerierOption {
	return &apply{fn: fn}
}

type order struct {
	order Ordering
}

func (a *order) filter(config *filterConfig) error {
	config.orders = append(config.orders, a.order)
	return nil
}

func (a *order) detail(*detailConfig) error {
	return nil
}

func WithPreload(columns ...string) QuerierOption {
	return &preload{columns: columns}
}

type preload struct {
	columns []string
}

func (a *preload) filter(config *filterConfig) error {
	config.preload = append(config.preload, a.columns...)
	return nil
}

func (a *preload) detail(config *detailConfig) error {
	config.preload = append(config.preload, a.columns...)
	return nil
}

func WithOrder(o Ordering) QuerierOption {
	return &order{order: o}
}

type op struct {
	name string
	f    FilterOp
}

func (o *op) filter(config *filterConfig) error {
	config.ops[o.name] = o.f
	return nil
}

func (o *op) detail(config *detailConfig) error {
	return nil
}

func WithOp(name string, f FilterOp) QuerierOption {
	return &op{name: name, f: f}
}

type unscoped struct {
	unscoped bool
}

func (a *unscoped) filter(config *filterConfig) error {
	config.unscoped = a.unscoped
	return nil
}

func (a *unscoped) detail(config *detailConfig) error {
	config.unscoped = a.unscoped
	return nil
}

// WithUnscoped will enable or disable unscoped mode on the generic.
// Enabled unscoped will allow deleted records in the response.
func WithUnscoped(v bool) QuerierOption {
	return &unscoped{unscoped: v}
}
