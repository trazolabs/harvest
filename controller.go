package harvest

import (
	"reflect"
	"strings"
)

func discoverViews(controller any) []*View {
	typeOf := reflect.TypeOf(controller)
	valueOf := reflect.ValueOf(controller)
	typeOfView := reflect.TypeOf(&View{})

	views := make([]*View, 0)

	for i := 0; i < typeOf.NumMethod(); i++ {
		methodType := typeOf.Method(i).Type
		methodValue := valueOf.Method(i)

		if methodType.NumIn() != 1 {
			continue
		}

		if methodType.NumOut() != 1 {
			continue
		}

		if methodType.Out(0) != typeOfView {
			continue
		}

		returnValues := methodValue.Call([]reflect.Value{})
		view := returnValues[0].Interface().(*View)
		views = append(views, view)
	}

	return sortViewPaths(views)
}

// sortViewPaths will sort views by their Path, resulting in routes that will most likely match unique requests.
func sortViewPaths(views []*View) []*View {
	return sortViewsGroupByMethod(views)
}

func matchRouteFirstDirectory(route []string) string {
	if len(route) == 0 || route[0] == "" {
		return "end"
	}

	for _, c := range route {
		if c[0] == '{' {
			return "dynamic"
		} else {
			return "static"
		}
	}

	return "end"

}

func groupViews(routes [][]string, depth int) ([][]string, [][]string, [][]string) {
	var end, dynamic, static [][]string

	for _, route := range routes {
		kind := matchRouteFirstDirectory(route[depth:])

		switch kind {
		case "end":
			end = append(end, route)
		case "static":
			static = append(static, route)
		case "dynamic":
			dynamic = append(dynamic, route)
		}
	}

	return static, dynamic, end
}

func sortRoutesWithDepth(routes [][]string, depth int) [][]string {
	if len(routes) == 0 {
		return [][]string{}
	}

	static, dynamic, end := groupViews(routes, depth)

	static = sortRoutesWithDepth(static, depth+1)
	dynamic = sortRoutesWithDepth(dynamic, depth+1)

	var result [][]string
	result = append(result, static...)
	result = append(result, dynamic...)
	result = append(result, end...)
	return result
}

func sortRoutes(routes []string) []string {
	// Split each route by "/".
	var splitRoutes [][]string
	for _, route := range routes {
		splitRoutes = append(splitRoutes, strings.Split(route, "/"))
	}

	var resultRoutes []string
	for _, route := range sortRoutesWithDepth(splitRoutes, 1) {
		resultRoutes = append(resultRoutes, strings.Join(route, "/"))
	}

	return resultRoutes
}

func internalSortViews(views []*View) []*View {
	routeViewMap := make(map[string]*View)
	routes := make([]string, 0, len(views))

	for _, view := range views {
		routeViewMap[view.path] = view
		routes = append(routes, view.path)
	}

	routes = sortRoutes(routes)

	resultViews := make([]*View, 0, len(routes))
	for _, route := range routes {
		resultViews = append(resultViews, routeViewMap[route])
	}

	return resultViews
}

func sortViewsGroupByMethod(views []*View) []*View {
	viewsByMethod := map[string][]*View{}

	for _, view := range views {
		if _, ok := viewsByMethod[view.method]; !ok {
			viewsByMethod[view.method] = make([]*View, 0)
		}

		viewsByMethod[view.method] = append(viewsByMethod[view.method], view)
	}

	resultViews := make([]*View, 0, len(views))
	for _, views := range viewsByMethod {
		resultViews = append(resultViews, internalSortViews(views)...)
	}

	return resultViews
}
