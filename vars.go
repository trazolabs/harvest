package harvest

import (
	"net/http"
	"net/url"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"github.com/monoculum/formam"
	uuid "github.com/satori/go.uuid"
)

type Vars struct {
	query url.Values
	path  map[string]string

	computed url.Values
}

func newVarsFromRequest(r *http.Request) *Vars {
	computed := r.URL.Query()
	pathVars := mux.Vars(r)

	// merge pathVars to url.Values format. This order is important,
	// PATH vars have precedence over QUERY vars.
	for key, value := range pathVars {
		computed[key] = []string{value}
	}

	return &Vars{
		query:    r.URL.Query(),
		path:     pathVars,
		computed: computed,
	}
}

// W returns a map[string]any for all values in the PATH of the request.
func (v *Vars) W() map[string]any {
	result := make(map[string]any)
	for key, value := range v.path {
		result[key] = value
	}
	return result
}

// W returns a map[string][]any for all values in the QUERY of the request.
func (v *Vars) F() map[string][]any {
	result := make(map[string][]any)
	for key, values := range v.query {
		result[key] = make([]any, len(values))
		for i, value := range values {
			if values[i] == "NULL" {
				result[key][i] = nil
			} else {
				result[key][i] = value
			}
		}
	}
	return result
}

func (v *Vars) Bind(to any) error {
	decoder := formam.NewDecoder(&formam.DecoderOptions{
		IgnoreUnknownKeys: true,
	})

	decoder.RegisterCustomType(func(vals []string) (any, error) {
		return time.Parse(time.RFC3339, vals[0])
	}, []any{time.Time{}}, nil)

	if err := decoder.Decode(v.computed, to); err != nil {
		return NewError(http.StatusBadRequest, err)
	}

	return nil
}

func (v *Vars) String(n string) (string, error) {
	values, ok := v.computed[n]
	if !ok {
		return "", NewErrorf(http.StatusBadRequest, "no such var: %s", n)
	}
	if len(values) == 0 {
		return "", NewErrorf(http.StatusBadRequest, "no such var: %s", n)
	}

	// This avoids multiple vars exploits on endpoints that
	// check auth on a query param.
	if len(values) > 1 {
		return "", NewErrorf(http.StatusBadRequest, "multiple var values: %s", n)
	}

	return values[0], nil
}

func (v *Vars) BindString(n string, to *string) error {
	id, err := v.String(n)
	if err != nil {
		return err
	}

	*to = id

	return nil
}

func (v *Vars) Strings(n string) ([]string, error) {
	values, ok := v.computed[n]
	if !ok {
		return nil, NewErrorf(http.StatusBadRequest, "no such var: %s", n)
	}
	return values, nil
}

func (v *Vars) UUID(n string) (uuid.UUID, error) {
	value, err := v.String(n)
	if err != nil {
		return uuid.Nil, err
	}
	id, err := uuid.FromString(value)
	if err != nil {
		return uuid.Nil, NewError(http.StatusBadRequest, err)
	}
	return id, nil
}

func (v *Vars) BindUUID(n string, to *uuid.UUID) error {
	id, err := v.UUID(n)
	if err != nil {
		return err
	}

	*to = id

	return nil
}

func (v *Vars) UUIDs(n string) ([]uuid.UUID, error) {
	values, err := v.Strings(n)
	if err != nil {
		return nil, err
	}
	result := make([]uuid.UUID, len(values))
	for i, v := range values {
		if asUUID, err := uuid.FromString(v); err != nil {
			return nil, err
		} else {
			result[i] = asUUID
		}
	}
	return result, nil
}

func (v *Vars) Int(n string) (int, error) {
	value, err := v.String(n)
	if err != nil {
		return 0, err
	}
	parsed, err := strconv.Atoi(value)
	if err != nil {
		return 0, NewError(http.StatusBadRequest, err)
	}
	return parsed, nil
}

func (v *Vars) Ints(n string) ([]int, error) {
	values, err := v.Strings(n)
	if err != nil {
		return nil, err
	}

	result := make([]int, 0, len(values))
	for _, v := range values {
		asInt, err := strconv.Atoi(v)
		if err != nil {
			return nil, NewError(http.StatusBadRequest, err)
		}
		result = append(result, asInt)
	}

	return result, nil
}

func (v *Vars) Bool(n string) (bool, error) {
	value, err := v.String(n)
	if err != nil {
		return false, err
	}
	parsed, err := strconv.ParseBool(value)
	if err != nil {
		return false, NewError(http.StatusBadRequest, err)
	}
	return parsed, nil
}

func (v *Vars) Float64(n string) (float64, error) {
	value, err := v.String(n)
	if err != nil {
		return 0, err
	}
	parsed, err := strconv.ParseFloat(value, 64)
	if err != nil {
		return 0, NewError(http.StatusBadRequest, err)
	}
	return parsed, nil
}

func (v *Vars) Float32(n string) (float32, error) {
	parsed, err := v.Float64(n)
	if err != nil {
		return 0, err
	}
	return float32(parsed), nil
}
