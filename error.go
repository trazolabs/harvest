package harvest

import (
	"errors"
	"fmt"
	"net/http"
)

type Error struct {
	Err        error
	StatusCode int
	data       map[string]any
}

func NewError(statusCode int, err error) error {
	return &Error{Err: err, StatusCode: statusCode, data: make(map[string]any)}
}

func NewErrorf(statusCode int, format string, args ...any) error {
	return &Error{Err: fmt.Errorf(format, args...), StatusCode: statusCode, data: make(map[string]any)}
}

// WithData replaces the data map for this error. This function accepts a Harvest Error, or any other error (transforming it to a Harvest Error with status code 500).
func WithData(err error, data map[string]any) error {
	var herr *Error
	if errors.As(err, &herr) {
		return &Error{Err: herr.Err, StatusCode: herr.StatusCode, data: data}
	}
	return &Error{Err: err, StatusCode: http.StatusInternalServerError, data: data}
}

// WithValue adds a single value to the data map via a shallow copy. Creates a Harvest Error if the error is not already one, with status code 500.
func WithValue(err error, key string, value any) error {
	var herr *Error
	if errors.As(err, &herr) {
		// Copy the map.
		newData := map[string]any{key: value}
		for k, v := range herr.data {
			newData[k] = v
		}

		return &Error{Err: herr.Err, StatusCode: herr.StatusCode, data: newData}
	}

	return &Error{Err: err, StatusCode: http.StatusInternalServerError, data: map[string]any{key: value}}
}

// Get a shallow copy of the data in this error.
func (ce *Error) Data() map[string]any {
	newData := map[string]any{}
	for k, v := range ce.data {
		newData[k] = v
	}
	return newData
}

func (ce Error) Error() string {
	return ce.Err.Error()
}

func (ce Error) Unwrap() error {
	return ce.Err
}

type httpResponseError struct {
	// StatusCode is the HTTP status of this server error.
	StatusCode int

	// Message is a humanized message for the client.
	Message string

	// Additional arbitrary data to include in the response.
	Data map[string]any `json:",omitempty"`
}

func (ce Error) toResponse() any {
	return &httpResponseError{
		StatusCode: ce.StatusCode,
		Message:    ce.Err.Error(),
		Data:       ce.data,
	}
}
