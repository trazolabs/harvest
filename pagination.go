package harvest

// Page represents the offset and limit of a requested page.
type Page struct {
	Offset int
	Limit  int
}

// PageResponse is a transport used when responding with a page.
type PageResponse[T any] struct {
	// Count of all existing items
	Count int

	// Items in this page
	Items []T
}

// List implements includer middleware Lister interface to unwrap items inside of this page and apply includers on it.
func (r *PageResponse[T]) List() any {
	return r.Items
}

func NewPageFromRequest(r *Request) *Page {
	offset, err := r.Vars().Int("offset")
	if err != nil {
		return nil
	}

	limit, err := r.Vars().Int("limit")
	if err != nil {
		return nil
	}

	return &Page{
		Offset: offset,
		Limit:  limit,
	}
}
