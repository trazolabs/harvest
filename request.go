package harvest

import (
	"context"
	"encoding/json"
	"errors"
	"log/slog"
	"net/http"
	"strings"

	"github.com/jinzhu/gorm"
	"github.com/monoculum/formam"
)

const defaultMaxMemory = 10 * 1024 * 1024

type Request struct {
	*http.Request

	logger      *slog.Logger
	middlewares []RawHandler
	vars        *Vars
	db          *gorm.DB
}

func wrapRequest(r *http.Request, view *View) *Request {
	middlewares := make([]RawHandler, 0, len(view.app.middlewares)+1)
	middlewares = append(middlewares, view.app.middlewares...)
	middlewares = append(middlewares, view.handler)

	return &Request{
		Request:     r,
		vars:        newVarsFromRequest(r),
		middlewares: middlewares,
		db:          view.app.db,
		logger: view.app.logger.With(
			slog.String("url", r.URL.String()),
			slog.String("method", r.Method),
		),
	}
}

// WithValue returns a shallow copy of the request, with a value set in the context.
func (r *Request) WithValue(key any, value any) *Request {
	return &Request{
		Request:     r.WithContext(context.WithValue(r.Context(), key, value)),
		vars:        r.vars,
		middlewares: r.middlewares,
		db:          r.db,
	}
}

func (r *Request) SetLogger(logger *slog.Logger) {
	r.logger = logger
}

func (r *Request) DB() *gorm.DB {
	return r.db
}

func (r *Request) CloneDB() *gorm.DB {
	newDB := r.db.New()
	r.db = newDB
	return r.db
}

func (r *Request) Vars() *Vars {
	return r.vars
}

func (r *Request) Logger() *slog.Logger {
	return r.logger
}

func (r *Request) AddLogAttrs(attrs ...any) {
	r.logger = r.logger.With(attrs...)
}

func (r *Request) HeaderStringDefault(name, def string) string {
	if v := r.Header.Get(name); v != "" {
		return v
	}
	return def
}

// Bind decodes one struct from the request.
func (r *Request) Bind(instance any) error {
	// Decode the data depending on what the content type is.
	if err := r.selectBinder(instance); err != nil {
		return NewError(http.StatusBadRequest, err)
	}

	return nil
}

func (r *Request) Next() (any, error) {
	if len(r.middlewares) == 0 {
		return nil, NewError(http.StatusInternalServerError, errors.New("cannot call Next() from view"))
	}

	handler := r.middlewares[0]
	r.middlewares = r.middlewares[1:]
	return handler(r)
}

func (r *Request) selectBinder(instance any) error {
	switch contentType := r.Header.Get("Content-Type"); {
	case strings.Contains(contentType, "multipart/form-data"):
		// The request probably contains a file, parse the multipart form.
		if err := r.ParseMultipartForm(defaultMaxMemory); err != nil {
			return err
		}

		// Bind the form data to each instance.
		return r.bindFormData(instance)
	case strings.Contains(contentType, "application/x-www-form-urlencoded"):
		// Just a simple form, parse it.
		if err := r.ParseForm(); err != nil {
			return err
		}

		// Bind the form data to each instance.
		return r.bindFormData(instance)
	case strings.Contains(contentType, "application/json"):
		// Bind the WithJSON data to each instance.
		return r.bindJsonData(instance)
	}

	return nil
}

func (r *Request) bindFormData(instance any) error {
	decoder := formam.NewDecoder(&formam.DecoderOptions{
		IgnoreUnknownKeys: true,
	})

	if err := decoder.Decode(r.PostForm, instance); err != nil {
		return err
	}

	return nil
}

func (r *Request) bindJsonData(instance any) error {
	if err := json.NewDecoder(r.Body).Decode(instance); err != nil {
		return err
	}
	return nil
}
